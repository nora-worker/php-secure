<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Secure;

use Nora;

/**
 * SecureModuleのテスト
 *
 */
class SecureTest extends \PHPUnit_Framework_TestCase
{
    public function testCaptcha ( )
    {
        ob_Start();
        Nora::Secure_Captcha( )->render(function($code) {
            ob_end_clean();
            var_dump($code);
            ob_start();
        });
        ob_end_clean();

    }

    public function testWeb ( )
    {
        // ロガーをスタートする
        Nora::Logging_start(null, 'debug');

        // PHP イベントを制御
        Nora::Environment_register();



        // Salt Md5
        list($password, $salt[]) = Nora::module('secure')->SaltMd5('hoge');
        list($password, $salt[]) = Nora::module('secure')->SaltMd5('deganjue');
        list($hogeta_password, $salt[]) = Nora::module('secure')->SaltMd5('hogeta');
        list($password, $salt[]) = Nora::module('secure')->SaltMd5('kogeta');

        $this->assertFalse( Nora::Secure_SaltMd5Verify('deganjue', $password, $salt));
        $this->assertTrue(  Nora::Secure_SaltMd5Verify('kogeta', $password, $salt));
        $this->assertTrue( Nora::Secure_SaltMd5Verify('hogeta', $hogeta_password, $salt));


        // CSRF用のストレージ
        //
        // $spec = 'file:///tmp/hoge';
        // $spec = 'mysql://root@degaunjue/Nora/CSRF';
        // $spec = 'redis://root@degaunjue/Nora/CSRF';
        //
        // Nora KVS:
        // - リアルタイムかスクリプト終了時か <-- データ保存タイミング
        // $key $value $time $meta
        Nora::Secure_CSRF()->setStorage($array);

        Nora::Secure_CSRF()->token($token);


        $this->assertFalse(Nora::Secure_Csrf()->check(''));
        $this->assertTrue(Nora::Secure_Csrf()->check($token));
        $this->assertFalse(Nora::Secure_Csrf()->check('hogehoge'));

    }

}
