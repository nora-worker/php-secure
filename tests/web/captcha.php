<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
require_once realpath(__DIR__.'/../..').'/vendor/autoload.php';


define('TEST_PROJECT_PATH', __DIR__);

session_start();

header('Content-Type: image/png');

Nora::Secure_Captcha( )->render(function($code) {
    $_SESSION['code'] = $code;
});

