<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Secure;

use Nora\Core\Component\Component;

/**
 * CSRF対策
 */
class CSRF extends Component
{
    private $_length = 32;
    private $_storage = [];

    protected function initComponentImpl( )
    {
    }
    /**
     * ストレージへ
     */
    public function setStorage(&$storage)
    {
        $this->_storage =& $storage;
        return $this;
    }

    /**
     * トークンを発行してストレージへ格納
     *
     * @param array
     * @param out
     */
    public function token (&$token)
    {
        // トークンを発行
        $token = $this->Secure_randomString($this->_length);
        $this->_storage[] = $token;
        return true;
    }

    /**
     * トークンを保有しているかの確認
     */
    public function check ($token)
    {
        if (!empty($token) && false !== array_search($token, $this->_storage))
        {
            return true;
        }
        return false;
    }
}
