<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Secure;

use Nora\Core\Component\Component;

/**
 * Captcha対策
 */
class Captcha extends Component
{
    const ANGLE_MIN = 0;
    const ANGLE_MAX = 10;
    const FONT_SIZE_MAX = 32;
    const FONT_SIZE_MIN = 16;

    private $_backgrounds = [];
    private $_fonts = [];

    protected function initComponentImpl( )
    {
        if( !function_exists('gd_info') ) {
            $this->error('Required GD library is missing');
        }

        $d = dir(realpath(__DIR__.'/..').'/backgrounds');

        while($f = $d->read())
        {
            if ($f[0] === '.') continue;
            $this->_backgrounds[] = $f;
        }

        $d = dir(realpath(__DIR__.'/..').'/fonts');

        while($f = $d->read())
        {
            if ($f[0] === '.') continue;
            $this->_fonts[] = $f;
        }
    }

    private function pickBackground( )
    {
        return realpath(__DIR__.'/..').
            '/backgrounds/'.
            $this->_backgrounds[rand(0, count($this->_backgrounds)-1)];
    }

    private function pickFont( )
    {
        return realpath(__DIR__.'/..').
            '/fonts/'.
            $this->_fonts[rand(0, count($this->_fonts)-1)];
    }

    private function hex2rgb ($color)
    {
        $color = str_replace('#', '', $color);
        if (strlen($color) != 6){ return array(0,0,0); }
        $rgb = array();
        for ($x=0;$x<3;$x++){
            $rgb[$x] = hexdec(substr($color,(2*$x),2));
        }
        return $rgb;

    }

    public function render($cb = null, $length = 7)
    {
        $bg = $this->pickBackground();
        $font = $this->pickFont();
        $code = $this->Secure_RandomString($length, []);
        if ($cb !== null)
        {
            $cb($code);
        }
        $font_color = '#666666';
        $font_shadow_color = '#ffffff';

        list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($bg);

        # 土台作成
        $captcha = imagecreatefrompng($bg);

        # 色作成
        list($r, $g, $b) = $this->hex2rgb($font_color);
        $color = imagecolorallocate($captcha, $r, $g, $b);

        # 角度
        $angle = rand( self::ANGLE_MIN, self::ANGLE_MAX ) * (rand(0, 1) == 1 ? -1 : 1);

        # フォントサイズ
        $size =  rand( self::FONT_SIZE_MIN, self::FONT_SIZE_MAX );
        $text_box_size = imagettfbbox($size, $angle, $font, $code);

        $box_width = abs($text_box_size[6] - $text_box_size[2]);
        $box_height = abs($text_box_size[5] - $text_box_size[1]);
        $text_pos_x_min = 0;
        $text_pos_x_max = ($bg_width) - ($box_width);
        $text_pos_x = rand($text_pos_x_min, $text_pos_x_max);
        $text_pos_y_min = $box_height;
        $text_pos_y_max = ($bg_height) - ($box_height / 2);
        $text_pos_y = rand($text_pos_y_min, $text_pos_y_max);

        list($r, $g, $b) = $this->hex2rgb($font_shadow_color);

        imagettftext($captcha,
            $size,
            $angle,
            $text_pos_x + -1,
            $text_pos_y + 1, 
            imagecolorallocate($captcha, $r, $g, $b),
            $font,
            $code);

        imagettftext($captcha,
            $size,
            $angle,
            $text_pos_x,
            $text_pos_y,
            $color,
            $font,
            $code);

        // Output image
        imagepng($captcha);
    }
}
